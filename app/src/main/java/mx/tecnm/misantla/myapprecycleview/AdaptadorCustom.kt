package mx.tecnm.misantla.myapprecycleview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdaptadorCustom(var context:Context, items:ArrayList<Platillo>): RecyclerView.Adapter<AdaptadorCustom.ViewHolder>() {

    var items:ArrayList<Platillo>? = null
    init {
        this.items = items
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdaptadorCustom.ViewHolder {
        val vista = LayoutInflater.from(context).inflate(R.layout.platillos,parent,false)
        val viewHolder = ViewHolder(vista)
        return  viewHolder
    }

    override fun onBindViewHolder(holder: AdaptadorCustom.ViewHolder, position: Int) {
      val item = items?.get(position)
        holder.foto?.setImageResource(item?.foto!!)
        holder.nombre?.text =item?.nombre
        holder.precio?.text = "$" +item?.precio.toString()
        holder.rating?.rating = item?.rating!!
    }

    override fun getItemCount(): Int {
        return items?.count()!!
    }

    class ViewHolder(vista: View):RecyclerView.ViewHolder(vista){
        var vista = vista
        var foto:ImageView? = null
        var nombre:TextView? = null
        var precio:TextView? = null
        var rating:RatingBar? = null

        init {
            foto = vista.findViewById(R.id.imgView)
            nombre = vista.findViewById(R.id.txtNombre)
            precio = vista.findViewById(R.id.txtPrecio)
            rating = vista.findViewById(R.id.ratingBar)
        }

    }
}