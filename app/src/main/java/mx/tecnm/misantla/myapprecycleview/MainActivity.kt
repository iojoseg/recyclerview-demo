package mx.tecnm.misantla.myapprecycleview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    var lista:RecyclerView? = null
    var adaptador:AdaptadorCustom? = null
    var layoutManager:RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val platillos = ArrayList<Platillo>()


        platillos.add(Platillo("Alitas Adobadas", 120.0,3.5F,R.drawable.platillo01))
        platillos.add(Platillo("ensalda Cesar", 70.0,4.5F,R.drawable.platillo02))
        platillos.add(Platillo("Bocadines", 60.0,1.5F,R.drawable.platillo03))
        platillos.add(Platillo("xxxx", 120.0,3.5F,R.drawable.platillo04))
        platillos.add(Platillo("xxxx", 70.0,4.5F,R.drawable.platillo05))
        platillos.add(Platillo("Bxxxx", 60.0,1.5F,R.drawable.platillo06))
        platillos.add(Platillo("xxxx", 120.0,3.5F,R.drawable.platillo07))
        platillos.add(Platillo("xxxx", 70.0,4.5F,R.drawable.platillo08))
        platillos.add(Platillo("Bxxxx", 60.0,1.5F,R.drawable.platillo09))

        lista = findViewById(R.id.Lista)
        lista?.setHasFixedSize(true)

        layoutManager = LinearLayoutManager(this)
        lista?.layoutManager = layoutManager

        adaptador = AdaptadorCustom(this,platillos)

        lista?.adapter = adaptador

    }
}